 processor 6502
 org $0810

 lda #$00   ; Black color into Accumulator register 
 sta $d021  ; Change window color to black(00)
 sta $d020  ; Change border color to black(00)


loaddccimage:
 lda $3f40,x
 sta $0400,x
 lda $4040,x
 sta $0500,x
 lda $4140,x
 sta $0600,x
 lda $4240,x
 sta $0700,x
 lda $4328,x
 sta $d800,x
 lda $4428,x
 sta $d900,x
 lda $4528,x
 sta $da00,x
 lda $4628,x
 sta $db00,x
 inx
 bne loaddccimage

 lda $4710   ;SID starts here		
 sta $d020
 sta $d021
 ldx #$00
 lda #$00
 tax
 tay
 jsr $1000
 sei
 lda #$7f
 sta $dc0d
 sta $dd0d
 lda #$01
 sta $d01a
 lda #$1b
 ldx #$08
 ldy #$14
 sta $d011
 stx $d016
 sty $d018
 lda #<irq
 ldx #>irq
 ldy #$7e
 sta $0314
 stx $0315
 sty $d012
 lda $dc0d
 lda $dd0d
 asl $d019
 cli

 lda #$3b
 sta $d011
 lda #$18
 sta $d016
 lda #$18
 sta $d018

loop:
 jmp loop

irq:      
 jsr $1003
 asl $d019
 jmp $ea81

 org $1000-$7e
 INCBIN "musicGremlins.sid"	

 org $1FFE
 INCBIN "basilImage.PRG"

 	 
		