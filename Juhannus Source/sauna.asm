;--------Juhannussauna C64-------------
;Aasipelit - 2016, SopuisaSopuli - 2020
;Compiled with DASM 

 processor 6502 ;Target processor
 org $0801  ;BASIC -merory area starts here
 .byte $0c,$08,$d0,$07,$9e,$20,$32,$30,$36,$34,$00,$00,$00,$00 ;Listing to call SYS2064 
 ; Can be generated using "Generate Sys() call" -tool in CBM Prg Studio

 org $0810  ;program starts at sys2064($0810)
 lda #%11111111  ; CIA#1 Port A needs to be set to input  
 sta $dc02       ; CIA#1 (Data Direction Register A)            
 lda #%00000000  ; CIA#1 Port B 
 sta $dc03       ; CIA#1 (Data Direction Register B)
 
startProgram:  
;-----------------------------------------------
 lda $DD00 ;VIC bank memory address
 and #%11111100
 ora #%00000010 ;Start from bank 1
 sta $DD00  
   
;-----------------------------------------------
 lda #$00 ;Load SID and start interrupt here	
 tax 
 tay
 jsr $C900 ;Music location 
 
 sei       ;Disable interrupts 
 lda #$01
 sta $d01a
 lda #$7f
 sta $dc0d
 sta $dd0d
 lda #<irqT1
 ldx #>irqT1
 sta $0314
 stx $0315
 ldy #$00
 sty $d012
 lda $dc0d
 lda $dd0d
 asl $d019
 cli       ;allow interrupts 
 
 ldx #$00 ;Set register X to 0 for copying data 
; Load and print title screen image
loadimage:
 lda $7F40,x ;Koala painter
 sta $4400,x ;Correct screen Ram location for image
 lda $8040,x
 sta $4500,x
 lda $8140,x
 sta $4600,x
 lda $8240,x
 sta $4700,x
 lda $8328,x ;Color data     
 sta $d800,x ;Copy color
 lda $8428,x
 sta $d900,x
 lda $8528,x
 sta $da00,x
 lda $8628,x
 sta $db00,x
 inx  ; Loop, if copy is not ready	
 bne loadimage
 
 lda #$3b   ;enter to bitmap mode
 sta $d011
 lda #$18   ;1=screen ram ($400) 8= memory location ($2000/$6000)
 sta $d016  ;enter to multicolor mode (d016)
 sta $d018  ;get the location of the bitmap (d018)
 
 lda #%0001101 ;Sprites 0,2,3 on /Sprites in multicolor mode
 sta $d015 ;sprite enable register
 sta $d01c ;sprite multicolor mode register
 
 lda #$1   ;Global color(white) shared between all sprites
 sta $d025 ;Global color 1
 sta $d026 ;Global color 2
 
 lda #$32  ;x=50 (flag)
 sta $d000 ;Sprite0 X-position
 lda #$50  ;y=80
 sta $d001 ;Sprite0 Y-position
 lda #$6   ;dark blue
 sta $d027 ;Sprite0 own color (dark blue)
  
 ;Get sprite pointers: (4800 -4000)/40 =20 
 lda #$22  ;Pointer Smoke cloud#1
 sta $47fa ;sprite2 (Correct RAM location at the end of screen RAM)
 sta $47fb ;sprite3
 lda #$f   ;gray
 sta $d029 ;sprite2 own color
 sta $d02A ;sprite3 own color
 
 lda #$16
 sta $d004
 sta $d006 
 lda #$30
 sta $d007 ; Position for Smoke clouds
 lda #%0001100 ;
 sta $d010  ;to access the right side of the screen
 
CloudDefault: ;Default position of the smoke cloud#1
 lda #$20 
 sta $47f8 ;Change the pointer of sprite0 (Flag)
 lda #$69
 sta $d005 ;New Y-position for smoke cloud
 jmp wait1
 
CloudDefault2: ;Default position of the smoke cloud#2
 lda #$21 
 sta $47f8
 lda #$69
 sta $d007 
 jmp MoveCloud2
  
wait1: 
 lda $d012 ;Load the current raster line into the accumulator
 cmp #$0A
 bcs wait1 ;repeat if rasterline ($d012) > #$0A
 bit $d011 ;Test raster with screen control register
 bmi wait1 ;Negative result, repeat
 
;Movement of the smoke clouds
MoveCloud:	
 ldy $d005  ; Load Index Y with Memory (Sprite2 Y-position)
 dey        ; Decrement Y-position
 sty $d005  ; Store Index Y in Memory
 cpy #$0    ; Check if sprite2 Y-position is 0
 beq CloudDefault
  
MoveCloud2:	
 ldy $d007  
 dey        
 sty $d007  
 cpy #$0   
 beq CloudDefault2
 
controls:
 lda #%01111111  ; Select row 8 (port 2)
 sta $dc00       
 lda $dc01       ; Read Port1 
 and #%00010000  ; If space key is pressed
 beq startingGame
 lda $dc00      ; Read Port2 
 and #$10       ; Check if button is pressed
 beq startingGame
 
 lda #%11011110  
 sta $dc00 		  
 lda $dc01         
 and #%00000100   
 beq startingGame 
 
 lda $dc00  	
 and #$08      
 beq startingGame 
 
 jmp wait1

startingGame:
 lda #%01111111   ;This loop here prevents moving to main game...
 sta $dc00        ;unless space and joy2 button are released
 lda $dc01         
 and #%00010000   ;This action prevents player characters drinking...
 beq startingGame ;animation right away after the game is started
 
 lda $dc00        ;There are more better ways to handle this...
 and #$10         ;but this is fine (for me atleast)......
 beq startingGame ;Well, moving on!
 
 lda #%11011110  ;Pressng "loyly" also works
 sta $dc00 		 ;Added for malfunctioning joysticks  
 lda $dc01         
 and #%00000100   
 beq startingGame 
 
 lda $dc00  	
 and #$08      
 beq startingGame 
 
 jmp startGame
;-------------------------------------------
;Interrupts for title screen:
;-------------------------------------------

irqT1:   
 
 lda #$03  ; Change the color for the upper...
 sta $d020 ; part of the screen border/BG
 sta $d021
 jsr $C903 ; Play audio ($C903 = play address (SidPlay))
  
 lda #<irqT2 ;Redirect interrupt
 ldx #>irqT2
 sta $0314
 stx $0315
 lda #$C9  ;Next interrupt starts at line #$C9
 sta $d012
 asl $d019 ;Clear Interrupt flag
 jmp $ea81 ;Jump back to standard interrupt service routine.
 
irqT2:  
 lda #$0D  ; Change the color for the lower...
 sta $d020 ; part of the screen border/BG
 sta $d021
 lda #<irqT1
 ldx #>irqT1
 sta $0314
 stx $0315
 lda #$00
 sta $d012
 asl $d019
 jmp $ea81 

;-----------------------------------------------------------------
;
;                    InGame screen
;
;-----------------------------------------------------------------

startGame:
 lda $DD00 
 and #%11111100
 ora #%00000011 ; VIC bank 0
 sta $DD00 
  
 lda #$01  
 sta $0286 ;Register for text color (white)
 jsr $E544 ;Clear screen with built-in sub routine
 ;(Text color won't change without clearing)
 ldx #$00
loadGameImage:
 lda $3F40,x
 sta $0400,x
 lda $4040,x
 sta $0500,x
 inx
 bne loadGameImage
 ldx #$00
loadGameImage2:
 lda $4140,x
 sta $0600,x
 inx
 cpx #$CF    	; Stop loading image before...
 bne loadGameImage2 ;the interrupts starting position
 
 ldx #$00
loadGameColor:
 lda $4328,x
 sta $d800,x
 lda $4428,x
 sta $d900,x 
 inx
 bne loadGameColor
 
 ldx #$00
clearInterruptError:  ; Error is based to text color (white)...
 lda #$00 			  ; so it needs to be changed to black
 sta $da00,x		  ; Location of error
 inx
 bne clearInterruptError
 
 lda #$00 ;Load SID and start interrupt here	
 tax 
 tay
 jsr $C000 ;Music location 
 
 sei
 lda #$01
 sta $d01a
 lda #$7f
 sta $dc0d
 sta $dd0d
 
 lda #$1b	
 sta $d011
 lda #$08
 sta $d016
 lda #$14
 sta $d018
 ;jsr enterTextMode

 lda #<irq
 ldx #>irq
 sta $0314
 stx $0315
 
 ldy #$00
 sty $d012
 
 lda $dc0d
 lda $dd0d
 asl $d019

 cli
 
 ;---------------------------------------------------------------
 ;Global colors shared between all sprites:
 lda #$0   ;black
 sta $d025 ;Global color 1
 lda #$1   ;white
 sta $d026 ;Global color 2
 
 lda #$ff ; All sprites on /Sprites in multicolor mode
 sta $d015 ;sprite enable register
 sta $d01c ;sprite multicolor mode register
 lda #%0000000  
 sta $d010  
 
 ;sprite is 64 bytes/ 64 dec= $40 hex
 ;sprite set memory location starts at $C80 
 ;sprite1:  $C80/40 = $30
 ;sprite2: $30...
 
 lda #$3d  ;Steam sprite pointer,positions and color
 sta $07fc
 lda #$FE
 sta $d008 
 lda #$9
 sta $d009 
 lda #$0f
 sta $d02B 
 
 lda #$98 ;Player head position
 sta $d000
 lda #$52 
 sta $d001 
 lda #$81   ;Player right arm
 sta $d002 
 lda #$66
 sta $d003 
 lda #$AD ;Player left arm position
 sta $d004 
 lda #$66 
 sta $d005 
 
 lda #$3e ;Celsius Sign
 sta $07ff
 lda #$50
 sta $D00E
 lda #$38
 sta $D00F
 lda #$01
 sta $D02E
  
 ldx #OLUET       ; Reset variable values
 stx .OLUT_COUNT 
 ldx #DELAYDEFAULT
 stx .DELAY
 ldx #$00
 stx .PISTEET
 stx .PISTEET+1
 stx .PISTEET+2
 stx .JUOMAKERROIN
 stx .TASOTEXTPOS
 stx .TASO
 lda #$05
 sta KUUMUUS
 
resetHeat:   ;Drinking beer changes...
 lda #$1     ;Color for sprites and background
 sta $d021
 sta $d027
 sta $d028 
 sta $d029 
 sta .IHO 
 ldx #$0
 stx .KUUMUUSKERROIN 
 jsr randomizeHeat
 
spritesDefault:
 ; $07f8- Player head ($30-$33)
 ; $07f9- right arm   ($34,$37)
 ; $07fa- left arm    ($35,$39)
 ; $07fb- beer bottle ($36,$38)
 ; $07fc- Steam   ($3d)
 ; $07fd- ;nappo  ($3A)
 ; $07fe- water  ($3B-$3C)
 ; $07ff- Celsius sign  ($3e)
 ; pointer $3f is empty, used to hide sprites
 
 lda #$30  ;Pointer 
 sta $07f8 ;sprite0  (Correct RAM location at the end of screen RAM)

 lda #$34  ;Player right arm
 sta $07f9 
 lda #$35  ;Player left arm
 sta $07fa

 lda #$36  ;Beer bottle
 sta $07fb
 lda #$73 
 sta $d006 
 lda #$86
 sta $d007 
 lda #$2
 sta $d02A 
 
 lda #$3f  ;nappo,water (hidden)
 sta $07fd
 sta $07fe 
 lda #$CF   ;Default position of water
 sta $d00C
 lda #$C
 sta $d02C  ;water color (light blue)
 
 lda #$C5
 sta $d00A  ;nappo X-position
 lda #$66
 sta $d00B  ;nappo y-position
 lda #$03
 sta $d02D ;nappo color (grey)
 
;Main loop ---
 
MainLoopWait1:
 lda $d012
 cmp #$0A
 bcs MainLoopWait1
 bit $d011
 bmi MainLoopWait1
  
MainLoopSteam:
 ldy $d009
 cpy #$0
 beq MainLoopWait2
 dey
 sty $d009 
 
MainLoopWait2:
 lda $d012
 cmp #$0A
 bcc MainLoopWait2
  
;----------------------------------------------------- 
changeHeat: 
 ldx .KUUMUUS_COUNT 
 cpx #$00
 bne checkAction
 inc .KUUMUUSKERROIN ;Delay method for raising heat
 ldx .KUUMUUSKERROIN ;increment can't be compared (memory)
 cpx #$3             ;so load index in memory X and compare
 bne checkAction
 jmp changeColor ;Jump because too far away for beq...

checkAction:
 dec .KUUMUUS_COUNT    
 lda $d00C  
 cmp #$CF   ;If there's loyly movement going
 bne moveLoyly ;Update it
 
 ldx .JUOMAKERROIN ; Is player drinking beer
 cpx #$00
 beq checkControls ;No, jump to check controls 
 jmp drinkBeer     ;Yes, jump to update method (again, too far away for bne...)
;--------------------------------------------------    
checkControls: 
 lda #%11011110  
 sta $dc00 		
 lda $dc01         
 and #%00000100 ;L-key
 beq loyly
 lda $dc00  	
 and #$08       ;Right
 beq loyly 
 
drinkCtrl: 
 lda #%01111111  
 sta $dc00 
 lda $dc01       ;Space-key  
 and #%00010000   
 beq checkDrink
 lda $dc00      
 and #$10       ;Joystick button
 beq checkDrink
;--------------------------------------------------   
mainLoopEnd: 
 dec .DELAY        ; Delay the score counter
 bne MainLoopWait1 ; If delay is not 0, reset to main loop
 lda #$10
 jsr addScore      ; Else, add ten points to the score...
 ldx #DELAYDEFAULT ; And reset delay value
 stx .DELAY
 jmp MainLoopWait1
;------------------------------------
loyly:   ; Start loyly
 lda $D009  ; Check if steam cloud if enough high...
 cmp #$40   ; So it won't get restared mid air
 bcs mainLoopEnd ; If not enough high, end loyly

 lda #$39  ;Left arm up 
 sta $07fa
 lda #$3A  ;NAPPO
 sta $07fd
 
 lda #$3B ;Water (upwards)
 sta $07fe 
 lda #$D1 ;New position for water sprite
 sta $d00C  
 lda #$56
 sta $d00D
 jmp mainLoopEnd
  
checkDrink: 
 ldx .OLUT_COUNT ;check if there's beer left
 cpx #$0 
 beq mainLoopEnd   ; No? End loop
 ldx #$37 		   ; Else, start drinking
 stx .JUOMAKERROIN  ; (Activates if "JUOMAKERROIN >0)
 dec .OLUT_COUNT    ; Remove beer
 jmp resetHeat
 
;----------------------------------------------------- 
moveLoyly:
 ldy $d00D  
 ldx $d00C  
 inx 
 stx $d00C ; Increase X-location (move right)
 cpx #$DC   
 bcs movedown ; If enough far away, change Y-movement
 dey
 sty $d00D  ; Decrease Y-location (move up)
 jmp mainLoopEnd
  
movedown:
 lda #$3C 
 sta $07fe    ; Sprite pointer, water moving downwards
 iny       
 sty $d00D    ; Move down (increase Y)
 cpy #$70     ; Return to main loop if not on destination (#$70)
 bcc mainLoopEnd
 lda #$72        ; Water hits the sauna stove, adjust steam location
 sta $d009 

 
 ldy #$0B   ;Each loyly decreases ".KUUMUUS_COUNT" 
loopHeating: 
 dec .KUUMUUS_COUNT 
 beq resetLoyly
 dey
 bne loopHeating

resetLoyly:
 lda #$99
 jsr addScore ; Update score(lda)
 lda #$99
 jsr addScore ;a dirty way to add 200 points     
 lda #$02
 jsr addScore       
 jmp spritesDefault 
 
;------------------------------------------------- 
 org $BFD ; load sprites at c00 + 3 bytes for color
 INCBIN "sprites.spr"
;-------------------------------------------------
  
drinkBeer:
 lda #$37   ;Right hand 
 sta $07f9
 lda #$38   ;Beer 
 sta $07fb
 lda #$80
 sta $d006  
 lda #$51
 sta $d007 ;New position for beer sprite

 ldx .JUOMAKERROIN
 TXA
 LSR
 bcs drinkBeer2 
 lda $07f8   ;Player head in drinking position
 cmp #$31
 beq swapAnim
 lda #$31 
 sta $07f8
 jmp drinkBeer2

swapAnim:
 lda #$32 
 sta $07f8

drinkBeer2:  ; Delay method to slow down 
 lda $d012   ; the drinking animation
 cmp #$0A   
 bcs drinkBeer2 
 bit $d011      
 bmi drinkBeer2
 
 ldy $d009      ; Avoid slowing down the
 cpy #$0        ; movement of the steam sprite
 beq beerWait2 
 dey
 sty $d009 
 
beerWait2:
 lda $d012
 cmp #$0A
 bcc beerWait2 
 
 dec .JUOMAKERROIN
 beq resetSprites ;If "JUOMAKERROIN" is 0, reset sprites
 jmp mainLoopEnd  ;Else, reset main loop

resetSprites:
 jmp spritesDefault
  
;---------------------------------------------------------
 
changeColor:
 ldx #$0
 stx .KUUMUUSKERROIN
 ldx .IHO 
 cpx #$2 
 beq jumpToGameOver ; If skin color is #$2 then jump to game over
 lda #$A            ; Get color to replace old color...
 cpx #$A            ; Check if skin color is already this color
 bne returnToLoop   ; If yes, jump
 lda #$2 ; If not, change it
 
returnToLoop:
 sta $d027   ; Color changes to...
 sta $d028   ; body sprites
 sta $d029   ; (Head and arms)
 sta .IHO    ; (Also for background color)
 jsr randomizeHeat
 jmp checkAction

jumpToGameOver:
 jmp gameOver 
;------------------------------------------

addScore:
 sta PISTERESET  ;Get score from loop
 jsr updateScore
 ldx .TASO
 lda .PISTEET+1 ;Checks "the middle"(?) byte

 cpx #$01   ;Hikoilija
 beq checkLvl2  
 cpx #$02    ;Saunoja
 beq checkLvl3
 
 cmp #$64        ; a scummy way to get the...
 bne endLoopNow  ; correct saunaLevel increment
 jsr getSaunaLevel 
 jmp endLoopNow

checkLvl2:
 cmp #$92
 bne endLoopNow
 jsr getSaunaLevel
 jmp endLoopNow

checkLvl3: 
 ldy .PISTEET+2  ;On the last level the lowest...
 cpy #$01        ;byte needs to be checked as well
 bne endLoopNow
 cmp #$95
 bne endLoopNow
 jsr getSaunaLevel
 
endLoopNow:
 rts

updateScore:
 sed ;Set decimal value on to compare score
 clc ;as a bcc value
 lda .PISTEET  ;Checks the highest byte
 adc PISTERESET
 sta .PISTEET
 bcc scoringDone ;If the score fits in to bytes
				 ;
 lda .PISTEET+1 
 adc #$00
 sta .PISTEET+1
 bcc scoringDone

 lda .PISTEET+2 ;Lowest bye (highest numbers on score)
 adc #$00
 sta .PISTEET+2

scoringDone:
 cld
 lda #$00
 sta PISTERESET
 
 lda .PISTEET+1
 cmp #$00
 bne changeImage
 lda $07ff
 cmp #$3e
 beq changeImage
 lda #$3e
 jmp endChange
changeImage: 
 lda #$3f 
endChange: 
 sta $07ff
jumpOver: 
 rts 

getSaunaLevel:  
 ldx .TASO         ; Check if current saunaLevel...
 cpx #$03          ; is less than 3
 beq levelSetupDone
 inc .TASO         ; if less, increment

 clc			   ; Clear carry
 ldx .TASOTEXTPOS  ; Get the current location of...
 TXA               ; saunaLevel text and perform addition
 adc #$0C 		   ; #$0C=12
 TAX
 stx .TASOTEXTPOS 
 
levelSetupDone:
 rts

;-------------------------------------------
;  Interrupts for inGame screen:
;-------------------------------------------

irq:  
 ldx .IHO  ;Get background based on...
 stx $d021 ;value of "IHO"
 jsr $C003 ;play SID
 lda #<irq2
 sta $0314
 lda #>irq2
 sta $0315
 lda #$C5
 sta $d012
 asl $d019
 lda #%00011000 ;
 sta $d016 
 lda #$3b 
 sta $d011
 lda #$18  
 sta $d018
 jmp $ea81
 
irq2: 
 lda #$00
 sta $d020
 sta $d021
 lda #<irq
 ldx #>irq
 sta $0314
 stx $0315
 lda #$00
 sta $d012
 jsr enterTextMode 

 ldx #$00       ;Zero= first character
displayBeerText: 
 lda olutText,x ;Get Text
 jsr swapChar   ;Subroutine that gets the right character
 sta $0700,x    ;Place text on wanted location
 inx            ;Increment, next character
 cpx #$06       ;How long the text is
 bne displayBeerText
  
 ldx #$00
 lda .OLUT_COUNT,x
 sec 
 sbc #$D0
 sta $0706,x

 ldy .TASOTEXTPOS
 ldx #$00
displayRanking:
 lda SAUNATASO,y
 jsr swapChar
 sta $0755,x
 inx
 iny
 cpx #$0C
 bne displayRanking
  
 ldx #$00 
displayInfo: 
 lda info,X
 jsr swapChar
 sta $07A3,x
 inx
 cpx #$3a
 bne displayInfo
 
 ldy #$05
 ldx #$00 
displayScore: 
 lda .PISTEET,x
 pha    ;push accumulator(PISTEET) on the stack
 and #$0f ;Save the lowest digit of score
 jsr positionChar
 pla  ;Retrieve higher nibble of the score
 lsr  ;and push it to the left side of the score
 lsr  ;Repeat to move all the bits to left
 lsr 
 lsr 
 jsr positionChar
 inx
 cpx #$03
 bne displayScore
 jmp finishInterrupt
positionChar:
 clc    
 adc #$30    ;Get correct character
 sta $070F,y ;and place it on the screen
 dey
 rts 
 
finishInterrupt:
 asl $d019
 jmp $ea81

;-----------------------------------------------------------------
;
;                    GameOver screen
;
;-----------------------------------------------------------------

gameOver: 
 lda #$00 ;Load SID and start interrupt here	
 tax 
 tay
 jsr $C400 ;Music location 
 
 sei
 lda #$01
 sta $d01a
 lda #$7f
 sta $dc0d
 sta $dd0d
 jsr enterTextMode
 lda #<irq4
 ldx #>irq4
 sta $0314
 stx $0315
 lda #$00
 sta $d012
 lda $dc0d
 lda $dd0d
 asl $d019
 cli
 
 ldx #$C5
resetTimer:      
 ldy #$C5
loopGameOver:
 dey
 bne loopGameOver
 
gameOverWait1:
 lda $d012
 cmp #$0A
 bcs gameOverWait1 ; Since the SID-file doesn't...
 bit $d011         ; have any "ending value".
 bmi gameOverWait1 ; Here's various ways of delaying 
gameOverWait2:     ; and timing the game over sequence
 lda $d012
 cmp #$0A
 bcc gameOverWait2
 dex 
 bne resetTimer    
 ; Once delay is over, move on to result screen
 ldx #$00 
 stx $d015 
 stx $d01c ; Hide sprites  
 stx $d021 ; and change colours
 stx $d020
 
 sei
clearScreen1:
 lda #$20     ;Fill screen with an empty spaces 
 sta $0400,x
 sta $0500,x
 sta $0600,x
 sta $0700,x  
 inx            ;Because of the spaces and the backgroud color...    
 bne clearScreen1 ;separate actions for the color memory aren't necessary
 jsr $e544
 jsr enterTextMode 
 
;PRINT TEXT:
 ldx #$00   
displayText: 
 lda msg,X
 jsr swapChar
 sta $0520,x
 inx
 cpx #$D
 bne displayText
 
 ldy .TASOTEXTPOS
 ldx #$00
displayRankingEnd:
 lda SAUNATASO,y
 jsr swapChar
 sta $052C,x
 inx
 iny
 cpx #$0C
 bne displayRankingEnd
 
 ldx #$00 
displayText1: 
 lda msg1,X
 jsr swapChar
 sta $0573,x
 inx
 cpx #$9
 bne displayText1

 ldx #$00 
displayText2: 
 lda msg3,X
 jsr swapChar
allowChar2:
 sta $05eb,x
 inx
 cpx #$E0
 bne displayText2
 
 ldy #$05
 ldx #$00 
displayScoreEnd: 
 lda .PISTEET,x
 pha
 and #$0f
 jsr positionCharEnd
 pla 
 lsr 
 lsr 
 lsr 
 lsr 
 jsr positionCharEnd

 inx
 cpx #$03
 bne displayScoreEnd
 jmp endScreen

positionCharEnd:
 clc    
 adc #$30  
 sta $057C,y
 dey
 rts   
 
endScreen:
 ldx #$FF
ScreenTimer:      
 ldy #$FF
loopScreenOver:
 dey
 bne loopScreenOver
OverScreenWait1:
 lda $d012
 cmp #$0A
 bcs OverScreenWait1 ; Since the SID-file doesn't...
 bit $d011         ; have any "ending value".
 bmi OverScreenWait1 ; Here's various ways of delaying 
OverScreenWait2:     ; and timing the game over sequence
 lda $d012
 cmp #$0A
 bcc OverScreenWait2
 dex 
 beq resetAll

 lda #%01111111 
 sta $dc00 
 lda $dc01         
 and #%00010000  
 beq startAgain
 lda $dc00      
 and #$10       
 beq startAgain
 
 lda #%11011110  
 sta $dc00 		  
 lda $dc01         
 and #%00000100   
 beq startAgain
 
 lda $dc00  	
 and #$08      
 beq startAgain
 
 jmp ScreenTimer
 
startAgain:
 lda #%01111111  
 sta $dc00        
 lda $dc01         
 and #%00010000   
 beq startAgain 
 lda $dc00        
 and #$10         
 beq startAgain 
 
 lda #%11011110  
 sta $dc00 		  
 lda $dc01         
 and #%00000100   
 beq startAgain
 
 lda $dc00  	
 and #$08      
 beq startAgain
 cli
 jmp startGame  

resetAll:
 jsr $e544
 jmp startProgram
 
;------------------------------------------------  
; Interrupts for Game over Screen 
;------------------------------------------------
irq4:
 lda #$33  ;Gameover, perform some changes
 sta $07f8 
 lda .IHO
 sta $d021 
 sta $d027
 sta $d028 
 sta $d029 
 jsr $C403 ;New SID
 lda #<irq5
 ldx #>irq5
 sta $0314
 stx $0315
 lda #$C5
 sta $d012
 asl $d019
 lda #%00011000 
 sta $d016
 lda #$3b 
 sta $d011
 lda #$18  
 sta $d018
 jmp $ea81
 
irq5:  
 lda #$00
 sta $d020
 sta $d021
 lda #<irq4
 ldx #>irq4
 sta $0314
 stx $0315
 lda #$00
 sta $d012
 jsr enterTextMode 
 asl $d019
 jmp $ea81
 
;-------------------------------------
; Sub-Routines
;------------------------------------- 
randomizeHeat:
 ;sed ;set decimal mode
 clc   ; Clear carry flag for new values
 lda #$04
 sta KUUMUUS  ;Add more heat based
 lda #KUUMUUS ;on current level
 ldx .TASO
 cpx #$01
 beq change1
 cpx #$02
 beq change2
 cpx #$03
 beq change2
 adc #$03
change1:
 adc #$02
change2:
 adc #$01
 sta .KUUMUUS_COUNT ;Reset heat count
 ;cld  ;Clear decimal mode
 rts
 
enterTextMode:
 lda #$1b  ;enter to text mode
 sta $d011
 lda #$08
 sta $d016
 lda #$14  ;DEFAULT C64 CHAR
 sta $d018
 rts
 
swapChar:
 cmp #$40         ;Check if char index is >40
 bcc returnToMain ;No, return
 sec			  ;yes, get the correct character 
 sbc #$40         ;avoids display PETSCII chars
returnToMain:
 rts
 
;-----------------------------------------------------------------
;
;                    Variables
;
;-----------------------------------------------------------------

PISTERESET = #$0 
.PISTEET:
 .byte 0,0,0

DELAYDEFAULT = #$50
.DELAY:
 .byte 0
 
OLUET = #$6
.OLUT_COUNT:
 .byte OLUET  
 
.JUOMAKERROIN:
 .byte 0 
  
KUUMUUS = #$05
.KUUMUUS_COUNT:
 .byte KUUMUUS 

.KUUMUUSKERROIN:
 .byte 0

.IHO:
 .byte 0 
 
.TASO:
 .byte 0 
 
.TASOTEXTPOS:
 .byte 0
;-------------------------------------------------
; All text strings are typed as CAPS to print them correctly
olutText:
 .byte "OLUET:"

info:  
 .byte "L/OIKEA - LOYLY                     ";40 
 .byte "SPACE/NAPPI - JUO OLUT";40
 
SAUNATASO:  ;length: 12 characters($0C) each
 .byte "ALOITTELIJA  HIKOILIJA    SAUNOJA   SAUNAMESTARI" ;12

; Text for Game over screen: 
msg:    
 .byte "SAUNATASOSI: "
 
msg1:    
 .byte "PISTEESI: "

msg3:  
 .byte "HYVAA JUHANNUSTA!                      ";40
 .byte "                                       ";40
 .byte "SPACE-SAUNO UUDELLEEN                  ";40 
 .byte "                                       ";40
 .byte "                                     ";38
 .byte "SOPUISASOPULI JA AASIPELIT-2020";31

;MESSAGE locations 0-4: $0520, $0573, $05eb, $0639, $0775

;-----------------------------------------------------------------
;
;                    Files 
;
;-----------------------------------------------------------------

;-----BANK 1 ($0000-3fff ) 
 
 org $1FFE
 INCBIN "juh1.PRG"
;-----BANK 2 ($4000-7fff)
 org $47FD
 INCBIN "Lippu.spr"
 org $5FFE
 INCBIN "intropic.PRG"
;-----BANK 3 ($8000-bFFF)
  
;-----BANK 4 ($c000-fffff )
 org $C000-$7e
 INCBIN "saunassa.sid"
 org $C400-$7e
 INCBIN "gameover.sid"
 org $C900-$7e
 INCBIN "intro.sid"
 